
import com.mycompany.calculadora.calculadora;
import com.mycompany.calculadora.Operaciones;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculadoraTest {

    Operaciones calculadora = new Operaciones();

    //Suma
    @Test
    public void TestSuma0() {
        assertEquals(6, calculadora.Suma(2, 4));
    }

    @Test
    public void TestSuma1() {
        assertEquals(18, calculadora.Suma(15, 3));
    }

    //Resta
    @Test
    public void TestResta0() {
        assertEquals(-6, calculadora.Resta(6, 12));
    }

    @Test
    public void TestResta1() {
        assertEquals(20, calculadora.Resta(29, 9));
    }

    //Multiplicacion
    @Test
    public void TestMultiplicacion0() {
        assertEquals(30, calculadora.Multiplicacion(15, 2));
    }

    @Test
    public void TestMultiplicacion1() {
        assertEquals(28, calculadora.Multiplicacion(7, 4));
    }

    //Division
    @Test
    public void TestDivision0() {
        assertEquals(2, calculadora.Division(20, 10));
    }

    @Test
    public void TestDivision1() {
        assertEquals(4, calculadora.Division(24, 6));
    }
}
